const fs = require('fs').promises;
const fetch = require('node-fetch');
const MongoClient = require('mongodb').MongoClient;
const url = 'mongodb://127.0.0.1:27017'

let db
const dbName = 'Full_info_pays'

async function getUserAsync(name) {
    let response = await fetch(`https://restcountries.eu/rest/v2/name/${name}`);
    let data = await response.json()
    return data;
}

MongoClient.connect(url, { useNewUrlParser: true }, (err, client) => {
    if (err) return console.log(err)
    db = client.db(dbName)

    const searchPays = async () => {

        const contentDeux = await fs.readFile('country_names.json');
        const jsonContentDeux = JSON.parse(contentDeux);

        for (let i = 0; i < jsonContentDeux.length; i++) {
            const element = jsonContentDeux[i];
            const fullElement = await getUserAsync(element)

            await db.collection("country_full_data").insert(fullElement, null, function (error, results) {
                if (error) throw error;
            });
        }
    }



    searchPays();


});