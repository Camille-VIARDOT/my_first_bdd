"use strict";

var fs = require('fs').promises;

var fetch = require('node-fetch');

var MongoClient = require('mongodb').MongoClient;

var url = 'mongodb://127.0.0.1:27017';
var db;
var dbName = 'Full_info_pays';

function getUserAsync(name) {
  var response, data;
  return regeneratorRuntime.async(function getUserAsync$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.next = 2;
          return regeneratorRuntime.awrap(fetch("https://restcountries.eu/rest/v2/name/".concat(name)));

        case 2:
          response = _context.sent;
          _context.next = 5;
          return regeneratorRuntime.awrap(response.json());

        case 5:
          data = _context.sent;
          return _context.abrupt("return", data);

        case 7:
        case "end":
          return _context.stop();
      }
    }
  });
}

MongoClient.connect(url, {
  useNewUrlParser: true
}, function (err, client) {
  if (err) return console.log(err);
  db = client.db(dbName);

  var searchPays = function searchPays() {
    var contentDeux, jsonContentDeux, i, element, fullElement;
    return regeneratorRuntime.async(function searchPays$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.next = 2;
            return regeneratorRuntime.awrap(fs.readFile('country_names.json'));

          case 2:
            contentDeux = _context2.sent;
            jsonContentDeux = JSON.parse(contentDeux);
            i = 0;

          case 5:
            if (!(i < jsonContentDeux.length)) {
              _context2.next = 15;
              break;
            }

            element = jsonContentDeux[i];
            _context2.next = 9;
            return regeneratorRuntime.awrap(getUserAsync(element));

          case 9:
            fullElement = _context2.sent;
            _context2.next = 12;
            return regeneratorRuntime.awrap(db.collection("country_full_data").insert(fullElement, null, function (error, results) {
              if (error) throw error;
            }));

          case 12:
            i++;
            _context2.next = 5;
            break;

          case 15:
          case "end":
            return _context2.stop();
        }
      }
    });
  };

  searchPays();
});