const fs = require('fs').promises;
const fetch = require('node-fetch');
const MongoClient = require('mongodb').MongoClient;
const url = 'mongodb://127.0.0.1:27017';

let db;
const dbName = 'Full_info_pays';



MongoClient.connect(url, { useNewUrlParser: true }, function (err, db) {
    if (err) throw err;
    var dbo = db.db("Full_info_pays");


    dbo.collection("country_full_data").find({ region: "Africa" }).toArray(function (err, result) {
        if (err) throw err;

        for (let i = 0; i < result.length; i++) {
            dbo.collection("Africa").insert(result[i], null, function (error, results) {
                if (error) throw error;
            });
        }
    });
    

    dbo.collection("country_full_data").find({ region: "Asia" }).toArray(function (err, result) {
        if (err) throw err;

        for (let i = 0; i < result.length; i++) {
            dbo.collection("Asia").insert(result[i], null, function (error, results) {
                if (error) throw error;
            });
        }
    });


    dbo.collection("country_full_data").find({ region: "Caribbean" }).toArray(function (err, result) {
        if (err) throw err;

        for (let i = 0; i < result.length; i++) {
            dbo.collection("Caribbean").insert(result[i], null, function (error, results) {
                if (error) throw error;
            });
        }
    });


    dbo.collection("country_full_data").find({ subregion: "Central America" }).toArray(function (err, result) {
        if (err) throw err;

        for (let i = 0; i < result.length; i++) {
            dbo.collection("Central America").insert(result[i], null, function (error, results) {
                if (error) throw error;
            });
        }
    });


    dbo.collection("country_full_data").find({ region: "Europe" }).toArray(function (err, result) {
        if (err) throw err;

        for (let i = 0; i < result.length; i++) {
            dbo.collection("Europe").insert(result[i], null, function (error, results) {
                if (error) throw error;
            });
        }
    });


    dbo.collection("country_full_data").find({ subregion: "Northem America" }).toArray(function (err, result) {
        if (err) throw err;

        for (let i = 0; i < result.length; i++) {
            dbo.collection("Northem America").insert(result[i], null, function (error, results) {
                if (error) throw error;
            });
        }
    });


    dbo.collection("country_full_data").find({ region: "Oceania" }).toArray(function (err, result) {
        if (err) throw err;

        for (let i = 0; i < result.length; i++) {
            dbo.collection("Oceania").insert(result[i], null, function (error, results) {
                if (error) throw error;
            });
        }
    });


    dbo.collection("country_full_data").find({ region: "Polar" }).toArray(function (err, result) {
        if (err) throw err;

        for (let i = 0; i < result.length; i++) {
            dbo.collection("Polar").insert(result[i], null, function (error, results) {
                if (error) throw error;
            });
        }
    });


    dbo.collection("country_full_data").find({ subregion: "South America" }).toArray(function (err, result) {
        if (err) throw err;

        for (let i = 0; i < result.length; i++) {
            dbo.collection("South America").insert(result[i], null, function (error, results) {
                if (error) throw error;
            });
        }
    });


  
});